package com.example.bank_cors.repositories;

import com.example.bank_cors.entities.BankAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankAccountRepository extends CrudRepository<BankAccount, String> {
    public List<BankAccount> findAllByCustomerId(int id);
    public BankAccount findBankAccountByAccountIdAndCustomerId(String accountId, int curstomerId);
}
