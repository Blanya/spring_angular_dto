package com.example.bank_cors.repositories;

import com.example.bank_cors.entities.AccountOperation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountOperationRepository extends CrudRepository<AccountOperation, Integer> {
    public List<AccountOperation> findAllByBankAccount_AccountIdOrderByOperationDateDesc(String id);
}
