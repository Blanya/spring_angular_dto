package com.example.bank_cors.controllers;

import com.example.bank_cors.dto.response.CustomerDto;
import com.example.bank_cors.services.CustomerService;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ebank/customer")
public class CustomerController {
    @Autowired
    private CustomerService _customerService;

    @GetMapping("")
    public ResponseEntity<List<CustomerDto>> getAll(){
        return ResponseEntity.ok(_customerService.getCustomers());
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerDto> getById(@PathVariable int id){
        CustomerDto  customerDto = _customerService.getCustomerById(id);
        if(customerDto != null)
            return ResponseEntity.ok(customerDto);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deleteById(@PathVariable int id){
        if(_customerService.deleteCustomerById(id))
            return new ResponseEntity<>(HttpStatus.OK);
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<CustomerDto> updateCustomer(@PathVariable int id, @RequestBody com.example.bank_cors.dto.request.CustomerDto customerDto){
        return ResponseEntity.ok(_customerService.updateCustomer(id, customerDto));
    }

    @PostMapping("/save")
    public ResponseEntity<CustomerDto> createCustomer(@RequestBody com.example.bank_cors.dto.request.CustomerDto customerDto){
        return ResponseEntity.ok(_customerService.addCustomer(customerDto));
    }

    @GetMapping("/search")
    public ResponseEntity<List<CustomerDto>> getCustomersByName(@RequestParam String keyword){
        return ResponseEntity.ok(_customerService.findCustomersByName(keyword));
    }
}
