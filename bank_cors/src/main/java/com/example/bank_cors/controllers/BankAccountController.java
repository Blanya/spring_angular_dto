package com.example.bank_cors.controllers;

import com.example.bank_cors.dto.request.AccountOperationDto;
import com.example.bank_cors.dto.response.BankAccountDto;
import com.example.bank_cors.services.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ebank/customer")
public class BankAccountController {
    @Autowired
    private BankAccountService _bankAccountService;

    @GetMapping("/{id}/account")
    public ResponseEntity<List<BankAccountDto>> getAllAccountsByCustomer(@PathVariable int id){
        return ResponseEntity.ok(_bankAccountService.getAccountsByClient(id));
    }

    @GetMapping("/{id}/account/{id_account}")
    public ResponseEntity<BankAccountDto> getAccountByIdCustomer(@PathVariable int id, @PathVariable String id_account){
        return ResponseEntity.ok(_bankAccountService.getAccountByCustomerIdAndAccountId(id, id_account));
    }

    @GetMapping("/account/{id_account}")
    public ResponseEntity<BankAccountDto> getAccountById(@PathVariable String id_account){
        return ResponseEntity.ok(_bankAccountService.getAccountById(id_account));
    }

    @PostMapping("/account/{id_account}")
    public ResponseEntity<BankAccountDto> addOperationAtAccount(@PathVariable String id_account, @RequestBody AccountOperationDto accountOperationDto){
        return ResponseEntity.ok(_bankAccountService.addOperationAtBankAccount(accountOperationDto, id_account));
    }
}
