package com.example.bank_cors.services;

import com.example.bank_cors.dto.response.AccountOperationDto;
import com.example.bank_cors.dto.response.BankAccountDto;
import com.example.bank_cors.entities.AccountOperation;
import com.example.bank_cors.entities.BankAccount;
import com.example.bank_cors.repositories.AccountOperationRepository;
import com.example.bank_cors.repositories.BankAccountRepository;
import com.example.bank_cors.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class BankAccountService {
    @Autowired
    private BankAccountRepository _bankAccountRepository;
    @Autowired
    private AccountOperationRepository _accountOperationRepository;
    @Autowired
    private Mapper _mapper;
    @Autowired
    private LinkService _linkService;

    public BankAccountDto createAccount(com.example.bank_cors.dto.request.BankAccountDto bankAccountDto, int id){
        BankAccount bankAccount = _mapper.fromBankAccountDto(bankAccountDto);
        BankAccount bankAccountSaved = _bankAccountRepository.save(bankAccount);
        BankAccountDto bankAccountDtoSaved = _mapper.fromBankAccount(bankAccountSaved);
        _linkService.addLinksToBankAccountDto(bankAccountDtoSaved);
        return bankAccountDtoSaved;
    }

    public List<BankAccountDto> getAccountsByClient(int id){
        List<BankAccountDto> bankAccountDtos = new ArrayList<>();
        List<BankAccount> bankAccounts = _bankAccountRepository.findAllByCustomerId(id);
        for (BankAccount bA: bankAccounts) {
            BankAccountDto bankAccountDto = _mapper.fromBankAccount(bA);
            //Recupérer les opérations des comptes du client
            List<AccountOperationDto> accountOperationDtos = getAllOperationsByAccount(bA.getAccountId());
            bankAccountDto.setAccountOperations(accountOperationDtos);

            _linkService.addLinksToBankAccountDto(bankAccountDto);
            bankAccountDtos.add(bankAccountDto);
        }
        return bankAccountDtos;
    }

    //Recupérer les opérations des comptes du client
    public List<AccountOperationDto> getAllOperationsByAccount(String id){
        List<AccountOperationDto> accountOperationDtos = new ArrayList<>();
        List<AccountOperation> accountOperations = _accountOperationRepository.findAllByBankAccount_AccountIdOrderByOperationDateDesc(id);
        for (AccountOperation accountOperation: accountOperations) {
            accountOperationDtos.add(_mapper.fromAccountOperation(accountOperation));
        }
        return accountOperationDtos;
    }

    public BankAccountDto getAccountByCustomerIdAndAccountId(int idCustomer, String idCompte){
        try{
            BankAccount bankAccount = _bankAccountRepository.findBankAccountByAccountIdAndCustomerId(idCompte, idCustomer);
            BankAccountDto bankAccountDto = _mapper.fromBankAccount(bankAccount);

            //Récupérer opérations du client
            bankAccountDto.setAccountOperations(getAllOperationsByAccount(idCompte));
            _linkService.addLinksToBankAccountDto(bankAccountDto);
            return bankAccountDto;
        }catch (Exception e){
            return null;
        }
    }

    public BankAccountDto getAccountById(String idCompte){
        try{
            BankAccount bankAccount = _bankAccountRepository.findById(idCompte).get();
            BankAccountDto bankAccountDto = _mapper.fromBankAccount(bankAccount);
            List<AccountOperationDto> accountOperationDtos = getAllOperationsByAccount(idCompte);
            bankAccountDto.setAccountOperations(accountOperationDtos);
            bankAccountDto.setTotalPages(Math.round(accountOperationDtos.size()/5));
            _linkService.addLinksToBankAccountDto(bankAccountDto);
            return bankAccountDto;
        }catch (Exception e){
            return null;
        }
    }

    public BankAccountDto addOperationAtBankAccount(com.example.bank_cors.dto.request.AccountOperationDto accountOperationDto, String id){
        try{
            AccountOperation accountOperation = _mapper.fromAccountOperationDto(accountOperationDto);
            BankAccount bankAccount= _bankAccountRepository.findById(id).get();
            accountOperation.setBankAccount(bankAccount);
            accountOperation.setOperationDate(new Date());
            AccountOperation accountOperationSaved = _accountOperationRepository.save(accountOperation);
            bankAccount.getAccountOperations().add(accountOperation);

            //Ajouter ou retirer l'argent du compte
             double amountBank = bankAccount.getBalance();
            if(accountOperation.getType().equals("DEBIT")){
                bankAccount.setBalance(amountBank- (accountOperation.getAmount()));
            } else if (accountOperation.getType().equals("CREDIT")) {
                bankAccount.setBalance(amountBank + (accountOperation.getAmount()));
            }
            BankAccount bankAccountUpdated = _bankAccountRepository.save(bankAccount);
            return _mapper.fromBankAccount(bankAccountUpdated);
        }catch (Exception e){
            return null;
        }
    }
}
