package com.example.bank_cors.services;

import com.example.bank_cors.controllers.BankAccountController;
import com.example.bank_cors.controllers.CustomerController;
import com.example.bank_cors.dto.response.AccountOperationDto;
import com.example.bank_cors.dto.response.BankAccountDto;
import com.example.bank_cors.dto.response.CustomerDto;
import com.example.bank_cors.entities.BankAccount;
import com.example.bank_cors.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;

@Service
public class LinkService {

    @Autowired
    private Mapper mapper;

    public void addLinksToCustomerDto(CustomerDto customerDto){
        List<Link> links = new ArrayList<>();
        links.add(linkTo(CustomerController.class).slash(customerDto.getId()).withSelfRel());
        links.add(linkTo(CustomerController.class).slash("delete").slash(customerDto.getId()).withRel("DELETE"));
        links.add(linkTo(CustomerController.class).slash("update").slash(customerDto.getId()).withRel("UPDATE"));

        for (Link l: links) {
            customerDto.add(l);
        }
    }

    public void addLinksToBankAccountDto(BankAccountDto bankAccountDto){
        List<Link> links = new ArrayList<>();
        links.add(linkTo(BankAccountController.class).slash(bankAccountDto.getAccountId()).withSelfRel());
        links.add(linkTo(BankAccountController.class).slash(bankAccountDto.getAccountId()).withRel("DELETE"));
        links.add(linkTo(BankAccountController.class).slash(bankAccountDto.getAccountId()).withRel("UPDATE"));

        for (Link l: links) {
            bankAccountDto.add(l);
        }
    }

    public void addLinksToOperationsDto(AccountOperationDto accountOperationDto){
        List<Link> links = new ArrayList<>();
        links.add(linkTo(BankAccount.class).slash("{customer_id}/{bank_account_id}").slash(accountOperationDto.getId()).withSelfRel());
        links.add(linkTo(BankAccount.class).slash("{customer_id}/{bank_account_id}/delete").slash(accountOperationDto.getId()).withRel("DELETE"));
        links.add(linkTo(BankAccount.class).slash("{customer_id}/{bank_account_id}/update").slash(accountOperationDto.getId()).withRel("UPDATE"));

        for (Link l: links) {
            accountOperationDto.add(l);
        }
    }
}
