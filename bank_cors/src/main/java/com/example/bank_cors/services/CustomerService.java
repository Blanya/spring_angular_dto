package com.example.bank_cors.services;

import com.example.bank_cors.dto.response.CustomerDto;
import com.example.bank_cors.entities.Customer;
import com.example.bank_cors.repositories.CustomerRepository;
import com.example.bank_cors.utils.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService {
    @Autowired
    private CustomerRepository _customerRepository;
    @Autowired
    private Mapper _mapper;
    @Autowired
    private LinkService _linkService;

    public List<CustomerDto> getCustomers(){
        List<CustomerDto> customers = new ArrayList<>();
        //Ajouter chaque client a la liste
        _customerRepository.findAll().spliterator().forEachRemaining(customer -> customers.add(_mapper.fromCustomer(customer)));

        //Ajout des links
        for (CustomerDto c: customers) {
            _linkService.addLinksToCustomerDto(c);
        }
        return customers;
    }

    public CustomerDto getCustomerById(int id){
        try {
            Customer customer = _customerRepository.findById(id).get();
            CustomerDto customerDto = _mapper.fromCustomer(customer);
            _linkService.addLinksToCustomerDto(customerDto);
            return customerDto;
        }catch (Exception e){
            return null;
        }
    }

    public CustomerDto updateCustomer(int id, com.example.bank_cors.dto.request.CustomerDto customerDto){
        try {
            Customer customer = _customerRepository.findById(id).get();
            //mapper en customer depuis les infos reçues
            Customer customerUpdated = _mapper.fromCustomerDto(customerDto);
            //Set id
            customerUpdated.setId(customer.getId());
            //Récupérer customer updated
            Customer customerSaved = _customerRepository.save(customerUpdated);
            //Mapper et envoyer réponse
            return _mapper.fromCustomer(customerSaved);
        }catch (Exception e){
            throw new RuntimeException();
        }
    }

    public boolean deleteCustomerById(int id){
        try {
            _customerRepository.deleteById(id);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    public CustomerDto addCustomer(com.example.bank_cors.dto.request.CustomerDto customerDto){
        Customer customer = _mapper.fromCustomerDto(customerDto);
        Customer customerSaved = _customerRepository.save(customer);
        return _mapper.fromCustomer(customerSaved);
    }

    public List<CustomerDto> findCustomersByName(String name){
        List<CustomerDto> customerDtos = new ArrayList<>();
        List<Customer> customers = _customerRepository.findAllByNameContains(name);
        for (Customer c: customers) {
            CustomerDto customerDto = _mapper.fromCustomer(c);
            _linkService.addLinksToCustomerDto(customerDto);
            customerDtos.add(customerDto);
        }

        return customerDtos;
    }
}
