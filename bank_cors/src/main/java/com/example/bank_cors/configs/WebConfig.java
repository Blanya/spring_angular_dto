package com.example.bank_cors.configs;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
public class WebConfig implements WebMvcConfigurer {

    public void addCorsMappings(CorsRegistry registry){
        registry.addMapping("/**") //chemin
                .allowedOrigins("http://localhost:4200") //URL autorisée à effectuer des requêtes
                .allowedMethods("GET","POST", "DELETE") //Quelles requêtes autorisées
                .allowedHeaders("*") //Dans le header => tout
                .allowCredentials(false).maxAge(3600); // Durée 1h
    }

}
