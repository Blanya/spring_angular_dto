package com.example.bank_cors.utils;

import com.example.bank_cors.dto.response.AccountOperationDto;
import com.example.bank_cors.dto.response.BankAccountDto;
import com.example.bank_cors.dto.response.CustomerDto;
import com.example.bank_cors.entities.AccountOperation;
import com.example.bank_cors.entities.BankAccount;
import com.example.bank_cors.entities.Customer;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class Mapper {
    private ModelMapper _mapper = new ModelMapper();

    public CustomerDto fromCustomer(Customer customer){
        CustomerDto customerDto = _mapper.map(customer, CustomerDto.class);
        return customerDto;
    }

    public Customer fromCustomerDto(com.example.bank_cors.dto.request.CustomerDto customerDto){
        Customer customer = _mapper.map(customerDto, Customer.class);
        return customer;
    }

    public AccountOperation fromAccountOperationDto(com.example.bank_cors.dto.request.AccountOperationDto accountOperationDto){
        return _mapper.map(accountOperationDto, AccountOperation.class);
    }

    public AccountOperationDto fromAccountOperation(AccountOperation accountOperation){
        return _mapper.map(accountOperation, AccountOperationDto.class);
    }

    public BankAccountDto fromBankAccount(BankAccount bankAccount){
        return _mapper.map(bankAccount, BankAccountDto.class);
    }
    public BankAccount fromBankAccountDto(com.example.bank_cors.dto.request.BankAccountDto bankAccountDto){
        return _mapper.map(bankAccountDto, BankAccount.class);
    }
}
