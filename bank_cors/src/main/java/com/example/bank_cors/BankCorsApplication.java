package com.example.bank_cors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;

@SpringBootApplication
public class BankCorsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BankCorsApplication.class, args);
	}

}
