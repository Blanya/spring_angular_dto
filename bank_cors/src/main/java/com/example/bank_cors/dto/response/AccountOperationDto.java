package com.example.bank_cors.dto.response;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import java.util.Date;

@Data
public class AccountOperationDto extends RepresentationModel<AccountOperationDto> {
    private int id;
    private Date operationDate;
    private String type;
    private double amount;
//    private String description;
}
