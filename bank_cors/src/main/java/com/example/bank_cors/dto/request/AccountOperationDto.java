package com.example.bank_cors.dto.request;

import lombok.Data;

@Data
public class AccountOperationDto {
    private String type;
    private double amount;
    private String description;

}
