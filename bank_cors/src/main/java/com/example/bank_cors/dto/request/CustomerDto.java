package com.example.bank_cors.dto.request;

import lombok.Data;

@Data
public class CustomerDto {
    private String email;
    private String name;
}
