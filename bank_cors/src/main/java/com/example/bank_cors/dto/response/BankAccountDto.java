package com.example.bank_cors.dto.response;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

import java.util.List;

@Data
public class BankAccountDto extends RepresentationModel<BankAccountDto> {
    private String accountId;
    private double balance;
    private List<AccountOperationDto> accountOperations;
    private int totalPages;
}
