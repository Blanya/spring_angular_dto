package com.example.bank_cors.dto.response;

import lombok.Data;
import org.springframework.hateoas.RepresentationModel;

@Data
public class CustomerDto extends RepresentationModel<CustomerDto> {
    private int id;
    private String name;
    private String email;

}
