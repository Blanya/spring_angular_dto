package com.example.bank_cors.dto.request;

import lombok.Data;

@Data
public class BankAccountDto {
    private String accountId;
    private String type;
}
