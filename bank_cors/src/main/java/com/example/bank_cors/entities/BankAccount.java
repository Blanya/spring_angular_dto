package com.example.bank_cors.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "bank_account")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BankAccount {
    @Id
    @Column(unique = true, name = "id")
    private String accountId;
    @Column(length = 4)
    private String type;
    private double balance;
//    @Temporal(TemporalType.DATE)
    @Column(name = "created_at")
    private Date createdAt;
    private String status;
    @Column(name = "over_draft")
    private double overDraft;
    @Column(name = "interest_rate")
    private double interestRate;
    @ManyToOne
    @JoinColumn(name = "customer_id")
    private Customer customer;

    @OneToMany(mappedBy = "bankAccount", orphanRemoval = true, cascade = CascadeType.ALL)
    @EqualsAndHashCode.Exclude
    private Set<AccountOperation> accountOperations = new HashSet<>();
}
