import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {AccountsService} from "../services/accounts.service";
import {Observable, throwError} from "rxjs";
import {AccountDetails, AccountOperation} from "../model/account.model";
import {catchError} from "rxjs/operators";

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {

  accountFormGroup! : FormGroup;
  currentPage : number =0;
  pageSize : number =5;
  accountObservable! : Observable<AccountDetails>
  operationFromGroup! : FormGroup;
  errorMessage! :string;
  operations: AccountOperation[] = [];
  operationsToRead : AccountOperation[] = [];
  i: number = 0;
  j: number = 0;

  constructor(private fb: FormBuilder, private accountService: AccountsService) { }

  ngOnInit(): void {
    this.accountFormGroup=this.fb.group({
      accountId : this.fb.control('')
    });
    this.operationFromGroup=this.fb.group({
      type : this.fb.control(null),
      amount : this.fb.control(0),
      description : this.fb.control(null),
      accountDestination : this.fb.control(null)
    })
  }

  async handleSearchAccount() {
    let accountId: string = this.accountFormGroup.value.accountId;
    this.accountObservable = this.accountService.getAccount(accountId,this.currentPage).pipe(
      catchError(err => {
        this.errorMessage=err.message;
        return throwError(err);
      })
    )
     this.accountObservable.subscribe( x =>  this.operations =  x.accountOperations);

    this.gotoPage(0);
  }

  gotoPage(page: number) {
    this.currentPage = page;
    // Boucle pour gérer l'affichage de 5 elements max
    if(this.currentPage == 0){
      this.i=0, this.j=5;
    }
    else{
      this.i = this.currentPage * 5;
      this.j = this.i + 5;     
    }

    let k = 0;
    let index = this.i;
    //tableau des op a afficher

    do{
      this.operationsToRead[k] = this.operations[index];
      index++;
      k++;
    }while(index < this.j)
  }
    

  handleAccountOperation() {
    // let accountId :string = this.accountFormGroup.value.accountId;
    // let operationType: string=this.operationFromGroup.value.operationType;
    // let amount :number =this.operationFromGroup.value.amount;
    // let description :string =this.operationFromGroup.value.description;
    // let accountDestination :string =this.operationFromGroup.value.accountDestination;

    let op: AccountOperation = this.operationFromGroup.value;
    let accountId: string = this.accountFormGroup.value.accountId;
    // op.type = this.operationFromGroup.get("type")?.value;
    this.accountService.saveOperation(op, accountId).subscribe( {
      next : data => {
        alert("Operation has been successfully saved!");
        this.operationFromGroup.reset();
        this.handleSearchAccount();
      },
      error : err => {
        console.log(err);
      }
    });
    // if(operationType=='DEBIT'){
    //   this.accountService.debit(accountId, amount,description).subscribe({
    //     next : (data)=>{
    //       alert("Success Credit");
    //       this.operationFromGroup.reset();
    //       this.handleSearchAccount();
    //     },
    //     error : (err)=>{
    //       console.log(err);
    //     }
    //   });
    // }else if(operationType=='CREDIT'){
    //   this.accountService.credit(accountId, amount,description).subscribe({
    //     next : (data)=>{
    //       alert("Success Debit");
    //       this.operationFromGroup.reset();
    //       this.handleSearchAccount();
    //     },
    //     error : (err)=>{
    //       console.log(err);
    //     }
    //   });
    // }else if(operationType=='TRANSFER'){
    //   this.accountService.transfer(accountId,accountDestination, amount,description).subscribe({
    //     next : (data)=>{
    //       alert("Success Transfer");
    //       this.operationFromGroup.reset();
    //       this.handleSearchAccount();
    //     },
    //     error : (err)=>{
    //       console.log(err);
    //     }
    //   });
    // }

  }



}
